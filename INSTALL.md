# Installing PreviewQtRemote

The instructions below serve as a guide to compile and install PreviewQtRemote from source. There are various means of installing PreviewQtRemote (Windows installer, Flatpak, etc.) listed on the website at https://previewqt.org/.

## DEPENDENCIES

- Qt >= 6.2 (6.5 or higher recommended)
- CMake >= 3.16

Make sure that you have all the required QML modules installed:
QtQuick, QtQuick.Controls, QtQuick.Window

## BUILDING AND INSTALLING

1. _mkdir build && cd build/_

2. _cmake .._

    \# Note: This installs PreviewQtRemote by default into /usr/local/{bin,share}
    \# To install PreviewQtRemote into another prefix e.g. /usr/{bin,share}, run:

    _cmake -DCMAKE\_INSTALL\_PREFIX=/usr .._

3. _make_

    \# This creates an executeable previewqtremote binary located in the ./build/ folder

4. (as root or sudo) _make install_

    \# This command:
    \# 1. is only required if you want to **install** PreviewQtRemote
    \# 2. installs the desktop file to share/applications/
    \# 3. moves some icons to icons/hicolor/
    \# 4. moves the binary to bin/
    \# 5. installs the metainfo file to share/metainfo/

## UNINSTALL

If you want to uninstall PreviewQtRemote, simply run __make uninstall__ as root. This removes the desktop file (via _xdg-desktop-menu uninstall_), the icons, the binary file, and the metainfo file. Alternatively you can simply remove all the files manually which should yield the same result.

## BUILDING ON WINDOWS

PreviewQtRemote offers installers for pre-built binaries on its website: https://previewqt.org/

If you prefer to build it yourself, this process is not as hard as it might seem at first. The main challenge in building PreviewQtRemote on Windows lies in getting the environment set up and all dependencies installed.

The following are required dependencies:

1. Install Visual Studio 2019 Community Edition (free, be sure to install the 'Desktop Development with C++' workload)
    - Website: https://visualstudio.microsoft.com/
2. Install CMake
    - Website: https://cmake.org/
    - In the installer set the system path option to Add CMake to the system PATH for all users
3. Install Qt 6.2 (6.5+ recommended)
    - Website: https://qt.io
    - In the installer, make sure to install all required modules as listed above under dependencies
    - After installation, confirm that your installation of Qt finds both CMake and the compiler installed in steps 1 and 2

Once all the dependencies are installed, then the source code of PreviewQtRemote can be fetched from the website (https://previewqt.og/). Then simply follow the instructions in the `BUILDING AND INSTALLING` section above.
