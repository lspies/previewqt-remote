# PreviewQtRemote

PreviewQtRemote is a **companion application to PreviewQt**. It allows for a easy and convenient way to pass files on to PreviewQt. Like PreviewQt it is based on Qt/QML, published as open-source, and completely free.

Visit its official website at https://previewqt.org/

## Features

PreviewQtRemote is a very simple application that is nothing but a small and frameless square that remains on top of all windows. It can be moved somewhere out of the way and dropping any file on that square makes PreviewQt pop up with that file loaded (if supported).

To get an overview of all supported file formats, check the website for PreviewQt: https://previewqt.org

#### NOTE

In order to use this application you first need to install PreviewQt.

## Download and Install

PreviewQtRemote can be installed in various ways. You can find a selection of them (Windows installer, Flatpak, etc.) [listed on the website: https://previewqt.org/](https://previewqt.org/)

Instructions of how to build PreviewQtRemote from source can be found in the INSTALL file in the root of the source directory.


## Contributing to PreviewQtRemote

There are **two easy ways** you can contribute directly to PreviewQtRemote:

1. **Join the translation team** over on [Crowdin](https://translate.photoqt.org/). If your language is missing you can simply request it directly on Crowdin or send me an email/message and I will add it to the project.
2. **Give feedback** about PreviewQtRemote. To do so, [send me an email](https://previewqt.org/about) or [open an issue on GitLab](https://gitlab.com/lspies/previewqt/-/issues/new).

If you want to support PreviewQtRemote financially, please **consider donating to the humanitarian relief in Ukraine** instead, for example to the [Ukrainian Red Cross](https://go.luspi.de/ukraine).


## License

PreviewQtRemote is released under the [GPLv2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) (or later) license.
